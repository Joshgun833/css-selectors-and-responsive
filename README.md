# How this task will be evaluated
## We expect your solution to the task to meet the following criteria:

- You have aligned the text and images of the header section to the center
- You have aligned the text of the grey section to the left
- You have aligned the text and images of the video section and footer to the center
- Your markup matches the design with the mobile representation breakpoint in 768px
- The header section doesn't contain all menu links, it contains only three lines
- You have aligned the font and indentations according to the initial design